package zad4;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

interface Observer<T>{
    void update(Observable<T> observable);
}

abstract class Observable<T>{
    private final ArrayList<Observer<T>> observers = new ArrayList<>();

    public void observe(Observer<T> observer) {
        if(!observers.contains(observer)) observers.add(observer);
    }
    public void unobserve(Observer<T> observer) {
        observers.remove(observer);
    }
    public void notifyObservers() {
        for (Observer<T> observer : observers) {
            observer.update(this);
        }
    }
}

class Song extends Observable<Song> implements Observer<Price> {
    public final int id;
    public final String title;
    public final String artist;
    public final int duration;
    public final int durationMin;
    private double pricePerSecond = 0.09;

    public Song(int id, String title, String artist, int duration){
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.durationMin = duration/60;
    }

    public Song(int id, String title, String artist, int duration, Price price){
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.durationMin = duration/60;
        this.pricePerSecond = price.getPricePerSecond();
        price.observe(this);
    }

    public void play(){
        System.out.println("Playing " + title + " - " + artist + " | Duration: " + durationMin + ":" + (duration-(60*durationMin)));
        notifyObservers();
    }

    @Override
    public void update(Observable<Price> observable) {
        Price price = (Price) observable;
        pricePerSecond = price.getPricePerSecond();
    }

    public void addPrice(Price price){
        pricePerSecond = price.getPricePerSecond();
        price.observe(this);
    }
    public int getId() {
        return id;
    }
    public int getDuration() {
        return duration;
    }
    public double getPricePerSecond() {
        return pricePerSecond;
    }
}

class Price extends Observable<Price>{
    private double pricePerSecond;

    public Price(double pricePerSecond){
        this.pricePerSecond = pricePerSecond;
    }

    public void updatePrice(double pricePerSecond) {
        this.pricePerSecond = pricePerSecond;
        notifyObservers();
    }

    public double getPricePerSecond() {
        return pricePerSecond;
    }
}

class Library implements Observer<Song> {
    private double cost = 0;
    private HashMap<Integer, Song> songs = new HashMap<>();

    public void add(Song song){
        songs.put(song.getId(), song);
        song.observe(this);
    }

    public void remove(int id) throws Exception {
        Song song = songs.remove(id);
        if(song != null){
            song.unobserve(this);
        } else {
            throw new Exception("No song with provided id.");
        }
    }

    @Override
    public void update(Observable<Song> observable) {
        Song song = (Song) observable;
        cost += song.getPricePerSecond() * song.getDuration();
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public String getCostString(){ return "Current cost: " + cost;}
}

public class ztp04 {
    public static void main(String[] args) throws Exception {
        Library lib = new Library();
        Song s1 = new Song(1, "Castle", "Halsey", 277);
        Song s2 = new Song(2, "Lean On", "STEPYN (feat. Krysta Youngs)", 147);
        Price s1price = new Price(0.19);
        Price s2price = new Price(0.51);

        System.out.println(" ========= Without adding to Library ========= ");
        s1.play();
        System.out.println(lib.getCostString());
        s2.play();
        System.out.println(lib.getCostString());

        System.out.println(" ========= Without custom pricing ========= ");
        lib.add(s1);
        lib.add(s2);
        s1.play();
        System.out.println(lib.getCostString());
        s2.play();
        System.out.println(lib.getCostString());

        System.out.println(" ========= With custom pricing ========= ");
        lib.setCost(0);
        s1.addPrice(s1price);
        s1.play();
        System.out.println(lib.getCostString());
        s2.addPrice(s2price);
        s2.play();
        System.out.println(lib.getCostString());

        System.out.println(" ========= With changed custom pricing ========= ");
        lib.setCost(0);
        s1price.updatePrice(0.25);
        s1.play();
        System.out.println(lib.getCostString());
        s2price.updatePrice(0.81);
        s2.play();
        System.out.println(lib.getCostString());
        s2.play();
        System.out.println(lib.getCostString());
        s2.play();
        System.out.println(lib.getCostString());

        System.out.println(" ========= Removed from Library ========= ");
        lib.setCost(0);
        lib.remove(1);
        lib.remove(2);
        s1.play();
        System.out.println(lib.getCostString());
        s2.play();
        System.out.println(lib.getCostString());
    }
}
