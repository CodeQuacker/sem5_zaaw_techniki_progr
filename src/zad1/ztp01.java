package zad1;

import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.*;

class Table
{
	// lista nagłówków
	private ArrayList<TableHeader> headers;
	// lista wierszy (każdy wiersz to lista komórek)
	private ArrayList<ArrayList<TableData>> rows;
	
	public Table()
	{
		headers = new ArrayList<TableHeader>();
		rows = new ArrayList<ArrayList<TableData>>();
	}
	// dodawanie wiersza
	public void addRow()
	{
		// nowy wiersz
		ArrayList<TableData> row = new ArrayList<TableData>();
		// wypełniamy komórkami - tyle, ile mamy kolumn
		for(TableHeader col:headers)
			row.add(col.returnData());
		// dodajemy do listy wierszy
		rows.add(row);
	}
	// dodawanie kolumny
	public void addCol(TableHeader header)
	{
		// dodajemy do listy nagłówków
		headers.add(header);
		// dodajemy po jednej komórce do każdego wiersza
		for(ArrayList<TableData> row:rows)
			row.add(header.returnData());
	}
	// do wypisywania tabeli
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		for(TableHeader header : headers)
			result.append("| " + header + " ");
		result.append("|\n");
		for(TableHeader header : headers) {
			result.append("+--");
			for (int i = 0; i < header.toString().length(); i++) {
				result.append("-");
			}
		}
		result.append("+\n");
		for(ArrayList<TableData> row : rows)
		{
			String data;
			for(TableData cell : row) {
				data = "| " + cell + " ";
				result.append(data);
				for (int i = 0; i < headers.get(row.indexOf(cell)).toString().length()-cell.toString().length(); i++) {
					result.append(" ");
				}
			}
			result.append("|\n");
		}
		return result.toString();
	}
}

interface TableData
{
	final static Random rnd = new Random();
}

class TableDataBoolean implements TableData
{
	private boolean data;
	public TableDataBoolean()
	{
		data = rnd.nextBoolean();
	}
	public String toString()
	{
		return Boolean.toString(data);
	}
}

class TableDataInt implements TableData
{
	private int data;
	public TableDataInt()
	{
		data = 10+rnd.nextInt(90);
	}
	public String toString()
	{
		return Integer.toString(data);
	}
}

class TableDataDouble implements TableData
{
	private double data;
	DecimalFormat df = new DecimalFormat("#.##");
	public TableDataDouble()
	{
		data = 10+rnd.nextDouble(90);
	}
	public String toString()
	{
		return df.format(data);
	}
}

class TableDataString implements TableData
{
	private String data;
	public TableDataString() {
		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 6;

		data = rnd.ints(leftLimit, rightLimit + 1)
				.limit(targetStringLength)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
				.toString();

	}
	public String toString() {
		return data;
	}
}

interface TableHeader
{
	TableData returnData();
}

class TableHeaderBoolean implements TableHeader
{
	private String name = "Boolean";
	public String toString()
	{
		return name;
	}

	@Override
	public TableData returnData() {
		return new TableDataBoolean();
	}
}

class TableHeaderInt implements TableHeader
{
	private String name = "Int";
	public String toString()
	{
		return name;
	}

	@Override
	public TableData returnData() {
		return new TableDataInt();
	}
}

class TableHeaderDouble implements TableHeader
{
	private String name = "Double";
	public String toString()
	{
		return name;
	}

	@Override
	public TableData returnData() {
		return new TableDataDouble();
	}
}

class TableHeaderString implements TableHeader
{
	private String name = "String";
	public String toString()
	{
		return name;
	}

	@Override
	public TableData returnData() {
		return new TableDataString();
	}
}



public class ztp01
{
	public static void main(String[] args)
	{
		Table table = new Table();
		table.addCol(new TableHeaderInt());
		table.addCol(new TableHeaderDouble());
		table.addCol(new TableHeaderString());

		table.addRow();
		table.addRow();
		table.addRow();
		table.addRow();
		table.addRow();
		System.out.println(table);


		table.addCol(new TableHeaderString());
		table.addCol(new TableHeaderBoolean());
		table.addRow();
		table.addRow();
		System.out.println(table);
	}
}
