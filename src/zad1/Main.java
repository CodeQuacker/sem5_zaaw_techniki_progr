package zad1;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    final static Random rnd = new Random();

    public static void main(String[] args) {
//        System.out.println("yo");
//        DecimalFormat df = new DecimalFormat("#.##");
//        double a = 10+rnd.nextDouble(90);
//        System.out.println(df.format(a));
//        int b = 20;
//        System.out.println(b);

        List<String> list = Stream.of("ccc", "aaa", "ddd", "bbb").toList();
//        list.forEach(System.out::println);

        List<String> limited = list.stream()
                .sorted()
                .limit(2)
                .toList();
        limited.forEach(System.out::println);


    }

}
