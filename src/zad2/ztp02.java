package zad2;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

interface MailServer{
    void addMessage(Message message);
    void printQueue();
    Message getMessage();
    int getQueueLength();
}

class Message{
    public String content = "no content";
    public String metadata = "";

    public Message(){}

    public Message(String content){
        this.content = content;
    }

    public Message(String content, String metadata){
        this.content = content;
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return metadata + content;
    }
}

class Counter{
    static int counter = 0;

    public static int getNextNumber(){
        counter += 1;
        return counter;
    }
}

class Queue implements MailServer {
    LinkedList<Message> queue = new LinkedList<>();

    @Override
    public void addMessage(Message message) {
        queue.addLast(message);
    }

    @Override
    public void printQueue() {
        int length = queue.size();
        for (int i = 0; i < length; i++) {
            System.out.println(queue.get(i));
        }
    }

    @Override
    public Message getMessage() {
        Message message;
        try {
            message = queue.removeFirst();
        }catch (Exception e){
            message = new Message();
            message.content = "Warning: No more messages. Error code: " + e;
        }
        return message;
    }

    public int getQueueLength(){
        return queue.size();
    }
}

abstract class Decorator implements MailServer{
    protected MailServer queue;
    public Decorator(MailServer queue){
        this.queue = queue;
    }

    @Override
    public void addMessage(Message message) {
        queue.addMessage(message);
    }

    @Override
    public Message getMessage() {
        return queue.getMessage();
    }

    @Override
    public int getQueueLength() {
        return queue.getQueueLength();
    }

    @Override
    public void printQueue() {
        queue.printQueue();
    }
}

class AddSendTime extends Decorator{

    public AddSendTime(MailServer queue) {
        super(queue);
    }

    @Override
    public void addMessage(Message message) {
        super.addMessage(addTime(message));
    }

    private Message addTime(Message message){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        message.metadata += "Sent: " + dateTime.format(formatter) + " | ";
        return message;
    }
}

class AddRecivedTime extends Decorator{
    public AddRecivedTime(MailServer queue) {
        super(queue);
    }

    @Override
    public Message getMessage() {
        return addTime(super.getMessage());
    }

    private Message addTime(Message message){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        message.metadata += "Recived: " + dateTime.format(formatter) + " | ";
        return message;
    }
}

class AddMessageNumber extends Decorator{
    public AddMessageNumber(MailServer queue) {
        super(queue);
    }

    @Override
    public void addMessage(Message message) {
        super.addMessage(addCounterNumber(message));
    }

    private Message addCounterNumber(Message message){
        message.metadata = "MessageId: " + Counter.getNextNumber()+ " | ";
        return message;
    }
}

class AddMessageFilter extends Decorator{
    ArrayList<String> filters = new ArrayList<>();
    public AddMessageFilter(MailServer queue, String filter) {
        super(queue);
        filters.add(filter);
    }

    public AddMessageFilter(MailServer queue, ArrayList<String> filters) {
        super(queue);
        this.filters = filters;
    }

    @Override
    public void addMessage(Message message) {
        super.addMessage(messageFilter(message));
    }

    private Message messageFilter(Message message){
        for (String filter :filters) {
            if (message.content.contains(filter)){
                message.content = "Message blocked. Reason: contains word \"" + filter + "\"";
                break;
            }
        }
        return message;
    }
}

class AddCensorFilter extends Decorator{
    ArrayList<String> filters = new ArrayList<>();
    public AddCensorFilter(MailServer queue, String filter) {
        super(queue);
        filters.add(filter);
    }

    public AddCensorFilter(MailServer queue, ArrayList<String> filters) {
        super(queue);
        this.filters = filters;
    }

    @Override
    public void addMessage(Message message) {
        super.addMessage(messageFilter(message));
    }

    private Message messageFilter(Message message){
        for (String filter :filters) {
            message.content = message.content.replaceAll(filter, "*".repeat(filter.length()));
        }
        return message;
    }
}

class AddEncryption extends Decorator{
    private final String encryptionKey = "verySecureEncryptionKey";
    private final StandardPBEStringEncryptor encryptor;

    public AddEncryption(MailServer queue) {
        super(queue);
        encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(encryptionKey);
    }

    @Override
    public void addMessage(Message message) {
        super.addMessage(encrypt(message));
    }

    @Override
    public Message getMessage() {
        return decrypt(super.getMessage());
    }

    private Message encrypt(Message message){
        message.content = encryptor.encrypt(message.content);
        return message;
    }

    private Message decrypt(Message message){
        try{
            message.content = encryptor.decrypt(message.content);
        }catch (Exception ignored){
        }
        return message;
    }
}

public class ztp02 {
    public static void main(String[] args) {
        ArrayList<String> censorFilters = new ArrayList<>(List.of(new String[]{"guns", "cigarette"}));
        ArrayList<String> messageFilters = new ArrayList<>(List.of(new String[]{"sale", "promo", "% off"}));

        System.out.println(" ================ messageMetadataQueue ================ ");
        MailServer messageMetadataQueue = new AddMessageNumber(new AddRecivedTime(new AddSendTime(new AddEncryption(new Queue()))));
        messageMetadataQueue.addMessage(new Message("Hello beautiful world"));
        messageMetadataQueue.addMessage(new Message("This sale is going good"));
        messageMetadataQueue.addMessage(new Message("Promotions still going on"));
        messageMetadataQueue.addMessage(new Message("This shop sell cigarettes"));
        messageMetadataQueue.printQueue();
        printQueue(messageMetadataQueue);

        System.out.println(" ================ censorQueue ================ ");
        MailServer censorQueue = new AddCensorFilter(messageMetadataQueue, censorFilters);
        censorQueue.addMessage(new Message("Hello beautiful world"));
        censorQueue.addMessage(new Message("This sale is going good"));
        censorQueue.addMessage(new Message("Promotions still going on"));
        censorQueue.addMessage(new Message("This shop sell cigarettes"));
        printQueue(censorQueue);

        System.out.println(" ================ blockedQueue ================ ");
        MailServer blockedQueue = new AddMessageFilter(messageMetadataQueue, messageFilters);
        blockedQueue.addMessage(new Message("Hello beautiful world"));
        blockedQueue.addMessage(new Message("This sale is going good"));
        blockedQueue.addMessage(new Message("Promotions still going on"));
        blockedQueue.addMessage(new Message("This shop sell cigarettes"));
        printQueue(censorQueue);

        System.out.println(" ================ encryptionQueue ================ ");
        MailServer encryptedQueue = new AddEncryption(new Queue());
        encryptedQueue.addMessage(new Message("Hello beautiful world"));
        encryptedQueue.addMessage(new Message("This sale is going good"));
        encryptedQueue.addMessage(new Message("Promotions still going on"));
        encryptedQueue.addMessage(new Message("This shop sell cigarettes"));
        System.out.println("encrypted");
        encryptedQueue.printQueue();
        System.out.println("decrypted");
        printQueue(encryptedQueue);
    }

    public static void printQueue(MailServer queue){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int length = queue.getQueueLength();
        for (int i = 0; i < length; i++) {
            System.out.println(queue.getMessage());
        }
    }
}
